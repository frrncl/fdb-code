/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.fdb.database;

import it.unipd.dei.fdb.resource.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Creates a new employee into the database.
 * 
 * @author Stefano Marchesin
 * @version 1.00
 */
public class CreateEmployeeDatabase {

	/**
	 * The SQL statement to be executed
	 */
	private static final String STATEMENT = "INSERT INTO Employee (badge, surname, age, salary) VALUES (?, ?, ?, ?)";

	/**
	 * The connection to the database
	 */
	private final Connection con;

	/**
	 * The employee to be stored into the database
	 */
	private final Employee employee;

	/**
	 * Creates a new object for storing an employee into the database.
	 * 
	 * @param con
	 *            the connection to the database.
	 * @param employee
	 *            the employee to be stored into the database.
	 */
	public CreateEmployeeDatabase(final Connection con, final Employee employee) {
		this.con = con;
		this.employee = employee;
	}

	/**
	 * Stores a new employee into the database
	 * 
	 * @throws SQLException
	 *             if any error occurs while storing the employee.
	 */
	public void createEmployee() throws SQLException {

		PreparedStatement pstmt = null;

		try {
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setInt(1, employee.getIdentifier());
			pstmt.setString(2, employee.getSurname());
			pstmt.setInt(3, employee.getAge());
			pstmt.setInt(4, employee.getSalary());

			pstmt.execute();

		} finally {
			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

	}
}
