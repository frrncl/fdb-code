/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.fdb.example;

import it.unipd.dei.fdb.database.*;
import it.unipd.dei.fdb.resource.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 * Creates a new employee into the database. 
 * 
 * @author Stefano Marchesin
 * @version 1.00
 */
public class UseEmployee {

	public static void main(String[] args) {
	
		// create a pool of connections
		PoolProperties p = new PoolProperties();
		
		// setup connection properties
		p.setUrl("jdbc:postgresql://localhost/employees");
		p.setDriverClassName("org.postgresql.Driver");
		p.setUsername("");  // set postgresql username
		p.setPassword("");  // set postgresql password
		
		// setup the size of the pool
		p.setInitialSize(5);
		p.setMaxActive(20);
		p.setMaxIdle(10);
		p.setMinIdle(10);
		
		// setup how and when to test that a connection is still working
		p.setValidationQuery("SELECT 1");
		p.setTestOnBorrow(true);
		p.setTestOnReturn(false);
		p.setTestWhileIdle(false);


		// create a datasource based on that pool
		DataSource datasource = new DataSource();
		datasource.setPoolProperties(p);
		
		// Adds a new employee
		try {
			Employee e  = new Employee(198, "Rossignoli", 24, 59);;
			
			// creates a new object for accessing the database and stores the employee
			new CreateEmployeeDatabase(datasource.getConnection(), e).createEmployee();
			
			System.out.printf("%nNew employee successfully created.%n%s%n", e.toString());
			
		} catch(Exception e) {
			System.out.printf("Unable to add the employee: %s%n", e.getMessage());
		}

		// Search for employees
		try {
		
			// searches for employees with the specified salary
			List<Employee> el = new SearchEmployeeBySalaryDatabase(datasource.getConnection(), 48).searchEmployeeBySalary();
			
			System.out.printf("%nEmployee with salaries over 48 successfully searched. Found employees:%n");
			for(Employee e: el) {
				System.out.printf("- %s%n", e.toString());
			}
			
		} catch(Exception e) {
			System.out.printf("Unable to search the employees: %s%n", e.getMessage());
		}

		datasource.close();
	
	}
	
}
