/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.fdb.resource;


/**
 * Represents the data about an employee.
 * 
 * @author Stefano Marchesin
 * @version 1.00
 */
public class Employee {

	/**
	 * The identifier of the employee
	 */
	private final int id;

	/**
	 * The surname of the employee
	 */
	private final String surname;

	/**
	 * The age of the employee
	 */
	private final int age;

	/**
	 * The salary of the employee
	 */
	private final int salary;

	/**
	 * Creates a new employee
	 * 
	 * @param id
	 *            the identifier of the employee
	 * @param surname
	 *            the surname of the employee.
	 * @param age
	 *            the age of the employee.
	 * @param salary
	 *            the salary of the employee
	 */
	public Employee(final int id, final String surname, final int age, final int salary) {
		this.id = id;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
	}

	/**
	 * Returns the identifier of the employee.
	 * 
	 * @return the identifier of the employee.
	 */
	public int getIdentifier() {
		return id;
	}

	/**
	 * Returns the surname of the employee.
	 * 
	 * @return the surname of the employee.
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Returns the age of the employee.
	 * 
	 * @return the age of the employee.
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Returns the salary of the employee.
	 * 
	 * @return the salary of the employee.
	 */
	public int getSalary() {
		return salary;
	}
	
	/**
	 * Returns a string representation of the employee.
	 * 
	 * @return a string representation of the employee.
	 */
	public String toString() {
	
		StringBuilder sb = new StringBuilder("Employee: ");
		
		sb.append("id = ").append(id).append("; ");
		sb.append("surname = ").append(surname).append("; ");
		sb.append("age = ").append(age).append("; ");
		sb.append("salary = ").append(salary).append(".");
		
		return sb.toString();
		
	}

}
