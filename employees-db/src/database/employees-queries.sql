-- Queries on the employees database
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021, Università degli Studi di Padova, Italia


--
-- Find badge, surname, age, and salary of those employees earning more than 42
--

SELECT *
  FROM Employee
  WHERE salary > 42;


--
-- Find badge, surname, and age of those employees earning more than 42
--

SELECT badge, surname, age
  FROM Employee
  WHERE salary > 42;


--
-- Find badge of managers whose employees earn more than 42
--

SELECT M.manager
  FROM Employee AS E INNER JOIN Manage AS M ON E.badge = M.employee
  WHERE E.salary > 42;


--
-- Find surname and salary of managers whose employees earn more than 42
--

SELECT C.surname, C.salary
  FROM Employee AS E INNER JOIN Manage AS M ON E.badge = M.employee INNER JOIN Employee AS C ON M.manager = C.badge
  WHERE E.salary > 42;


--
-- Find surname and salary of managers earning more than 45
--

SELECT DISTINCT surname, salary
  FROM Employee AS E INNER JOIN Manage AS M ON E.badge = M.manager
  WHERE salary > 45;
  

--
-- Find employees who earn more than their manager, showing badge, surname, and salary of both the employee and their manager 
--

SELECT E.badge, E.surname, E.salary, C.badge, C.surname, C.salary
	FROM Employee AS E INNER JOIN Manage AS M ON E.badge = M.employee INNER JOIN Employee AS C ON M.manager = C.badge 
	WHERE E.salary > C.salary;
	

--
-- Find badges of managers whose employees earn all more than 42
--

SELECT manager
  FROM Manage
EXCEPT
SELECT manager
  FROM Employee AS E INNER JOIN Manage AS M ON E.badge = M.employee 
  WHERE E.badge <= 42;


--
-- Find badges of managers who have a manager themselves
--

SELECT DISTINCT M.employee
  FROM Manage AS M INNER JOIN Manage AS C ON M.employee = C.manager;

-- oppure

SELECT employee
  FROM Manage
INTERSECT
SELECT manager
  FROM Manage;

