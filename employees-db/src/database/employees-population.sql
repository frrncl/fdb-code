-- Population of the employees database
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021, Università degli Studi di Padova, Italia


--
-- Population of the Employee table
--
INSERT INTO Employee VALUES (7309, 'Rossi', 34, 45);
INSERT INTO Employee VALUES (5998, 'Bianchi', 37, 38);
INSERT INTO Employee VALUES (9553, 'Neri', 42, 35);
INSERT INTO Employee VALUES (5698, 'Bruni', 43, 42);
INSERT INTO Employee VALUES (4076, 'Mori', 45, 50);
INSERT INTO Employee VALUES (8123, 'Lupi', 46, 60);

--
-- Population of the Manage table
--
INSERT INTO Manage VALUES(7309, 5698);
INSERT INTO Manage VALUES(5998, 5698);
INSERT INTO Manage VALUES(9553, 4076);
INSERT INTO Manage VALUES(5698, 4076);
INSERT INTO Manage VALUES(4076, 8123);
