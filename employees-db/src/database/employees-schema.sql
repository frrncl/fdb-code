-- Creation of the employees database schema
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021, Università degli Studi di Padova, Italia


-- #################################################################################################
-- ## Database creation                                                                           ##
-- #################################################################################################

-- Delete pre-existent database instance (if any)
DROP DATABASE IF EXISTS Employees;

-- Create the database
CREATE DATABASE Employees ENCODING 'UTF-8';
COMMENT ON DATABASE Employees IS ' Employees example database.';


-- #################################################################################################
-- ## Database connection                                                                         ##
-- #################################################################################################

-- Connect to the database
\connect employees


-- #################################################################################################
-- ## Tables creation                                                                             ##
-- #################################################################################################

--
-- This table represents an employee
--
-- Version 1.00
CREATE TABLE Employee (
	badge INT,
	surname VARCHAR(50) NOT NULL,
	age INT,
	salary INT,
	PRIMARY KEY (badge)
);

COMMENT ON TABLE Employee IS 'Represents an employee.';
COMMENT ON COLUMN Employee.badge IS 'Badge number.';
COMMENT ON COLUMN Employee.surname IS 'Employee''s surname.';
COMMENT ON COLUMN Employee.age IS 'Employee''s age.';
COMMENT ON COLUMN Employee.salary IS 'Employee''s salary in thousands (euro).';

--
-- This table represents the manager of an employee
--
-- Version 1.00
CREATE TABLE Manage (
  employee INT NOT NULL,
  manager INT NOT NULL,
  PRIMARY KEY (employee),
  FOREIGN KEY (employee) REFERENCES Employee(badge),
  FOREIGN KEY (manager) REFERENCES Employee(badge)
);

COMMENT ON TABLE Manage IS 'Represents the supervisor of an employee.';
COMMENT ON COLUMN Manage.employee IS 'Employee''s badge number.';
COMMENT ON COLUMN Manage.manager IS 'Manager''s badge number.';


