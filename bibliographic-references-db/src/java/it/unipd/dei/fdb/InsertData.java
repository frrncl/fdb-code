/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unipd.dei.fdb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Reads from a CVS-like text file the data to be inserted into the database..
 * 
 * @author Stefano Marchesin
 * @version 1.00
 */
public class InsertData {

	/**
	 * The JDBC driver to be used
	 */
	private static final String DRIVER = "org.postgresql.Driver";
	
	/**
	 * The URL of the database to be accessed
	 */
	private static final String DATABASE = "jdbc:postgresql://localhost/bibliographicreferences";

	/**
	 * The username for accessing the database
	 */
	private static final String USER = "";  // insert the username here

	/**
	 * The password for accessing the database
	 */
	private static final String PASSWORD = "";  // insert the password here
	
	/**
	 * The default input file for reading the data
	 */
	private static final String DEFAULT_INPUT_FILE = "data.txt";
	
	/**
	 * The SQL statement for inserting data into the Author table.
	 */
	private static final String INSERT_INTO_AUTHOR = "INSERT INTO Author (id, name, surname) VALUES (?, ?, ?)";

	/**
	 * The SQL statement for inserting data into the Publisher table.
	 */	
	private static final String INSERT_INTO_PUBLISHER = "INSERT INTO Publisher (id, name) VALUES (?, ?)";

	/**
	 * The SQL statement for inserting data into the Reference table.
	 */
	private static final String INSERT_INTO_REFERENCE = "INSERT INTO Reference (id, title, year, publisher) VALUES (?, ?, ?, ?)";

	/**
	 * The SQL statement for inserting data into the Write table.
	 */
	private static final String INSERT_INTO_WRITE = "INSERT INTO Write (reference, author, AuthorNumber) VALUES (?, ?, ?)";

	/**
	 * The SQL statement for inserting data into the Information table.
	 */
	private static final String INSERT_INTO_INFORMATION = "INSERT INTO Information (reference, notes) VALUES (?, ?)";


	/**
	 * Reads from a CVS-like text file the data to be inserted into the database.
	 * 
	 * @param args
	 *            command-line arguments. If provided, {@code args[0]} contains
	 *            the name of the file to be read.
	 *            
	 */
	public static void main(String[] args) {

		// the connection to the DBMS
		Connection con = null;

		// the SQL statement to be executed
		PreparedStatement pstmtAuthor = null;
		PreparedStatement pstmtPublisher = null;
		PreparedStatement pstmtReference = null;
		PreparedStatement pstmtWrite = null;
		PreparedStatement pstmtInformation = null;
		
		// start time of a statement
		long start;

		// end time of a statement
		long end;
		
		// the input file from which data are read
		Reader input = null;
		
		// a scanner for parsing the content of the input file
		Scanner s = null;
		
		// current line number in the input file
		int l = 0;
		
		// "data structures" for the data to be inserted into the database
		
		// the number of authors of a reference
		int numAuthors = 0;

		// flag to indicate whether additional information are available
		boolean hasInfo = false;

		// the data about the authors of a reference
		String[] authorID = null;
		String[] authorFirstName = null;
		String[] authorLastName = null;


		// the data about the reference
		String referenceID = null;
		String referenceTitle = null;
		int referenceYear;

		// the data about the publisher of a reference
		String publisherID = null;
		String publisherName = null;

		// additional information about a reference
		String additionalInfo = null;

		
		try {
			// register the JDBC driver
			Class.forName(DRIVER);

			System.out.printf("Driver %s successfully registered.%n", DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.printf(
					"Driver %s not found: %s.%n", DRIVER, e.getMessage());

			// terminate with a generic error code
			System.exit(-1);
		}


		try {

			// connect to the database
			start = System.currentTimeMillis();			
			
			con = DriverManager.getConnection(DATABASE, USER, PASSWORD);								
			
			end = System.currentTimeMillis();

			System.out.printf(
					"Connection to database %s successfully established in %,d milliseconds.%n",
					DATABASE, end-start);

			// create the statements for inserting the data
			start = System.currentTimeMillis();
			
			pstmtAuthor = con.prepareStatement(INSERT_INTO_AUTHOR);
			pstmtPublisher = con.prepareStatement(INSERT_INTO_PUBLISHER);
			pstmtReference = con.prepareStatement(INSERT_INTO_REFERENCE);
			pstmtWrite = con.prepareStatement(INSERT_INTO_WRITE);
			pstmtInformation = con.prepareStatement(INSERT_INTO_INFORMATION);
			
			end = System.currentTimeMillis();
			
			System.out.printf(
					"Statements successfully created in %,d milliseconds.%n",
					end-start);

		} catch (SQLException e) {
			System.out.printf("Connection error:%n");

			// cycle in the exception chain
			while (e != null) {
				System.out.printf("- Message: %s%n", e.getMessage());
				System.out.printf("- SQL status code: %s%n", e.getSQLState());
				System.out.printf("- SQL error code: %s%n", e.getErrorCode());
				System.out.printf("%n");
				e = e.getNextException();
			}
			
			// terminate with a generic error code
			System.exit(-1);
		}

		
		// if there are no input arguments, use the default input file
		if (args.length == 0) {

			// get the class loader
			ClassLoader cl = InsertData.class.getClassLoader();
			if (cl == null) {
				cl = ClassLoader.getSystemClassLoader();
			}

			// Get the stream for reading the configuration file
			InputStream is = cl.getResourceAsStream(DEFAULT_INPUT_FILE);

			if (is == null) {
				System.out.printf("Input file %s not found.%n", DEFAULT_INPUT_FILE);
				
				// terminate with a generic error code
				System.exit(-1);
			}
			
			input = new BufferedReader(new InputStreamReader(is));
			
			System.out.printf("Input file %s successfully opened.%n", DEFAULT_INPUT_FILE);
			
		} else {
			
			try {
				input = new BufferedReader(new FileReader(args[0]));
				
				System.out.printf("Input file %s successfully opened.%n", args[0]);
			} catch (IOException ioe) {
				System.out.printf(
						"Impossible to read input file %s: %s%n", args[0],
						ioe.getMessage());
				
				// terminate with a generic error code
				System.exit(-1);
			}
		}

		// create the input file parser
		s = new Scanner(input);

		// set the delimiter for the fields in the input file
		s.useDelimiter("##");
	
		try {
			
			// while the are lines to be read from the input file
			while (s.hasNext()) {
				
				// increment the line number counter
				l++;
				
				System.out.printf("%n--------------------------------------------------------%n");
				
				// read one line from the input file
				start = System.currentTimeMillis();
				
				// number of authors of a reference
				numAuthors = s.nextInt();

				// determine whether a reference has additional info or not
				hasInfo = (s.nextInt() == 1);

				// create the arrays for containing author data
				authorID = new String[numAuthors];
				authorFirstName = new String[numAuthors];
				authorLastName = new String[numAuthors];


				// read the authors
				for (int i = 0; i < numAuthors; i++) {
					authorFirstName[i] = s.next();
					authorLastName[i] = s.next();
					
					// take the hash of the author last name as id
					authorID[i] = Integer
							.toHexString(authorLastName[i].hashCode());
				}

				// read the title of a reference
				referenceTitle = s.next();
				
				// take the hash of the title as id
				referenceID = Integer.toHexString(referenceTitle.hashCode());

				// read the publisher
				publisherName = s.next();
				
				// take the hash of the publisher name as id
				publisherID = Integer.toHexString(publisherName.hashCode());

				// read the year of a reference
				referenceYear = s.nextInt();

				// read the additional information, if any
				if (hasInfo) {
					additionalInfo = s.next();
				}

				// go to the next line
				s.nextLine();
				
				end = System.currentTimeMillis();
				
				System.out.printf(
						"Line %,d successfully read in %,d milliseconds.%n",
						l, end-start);

				
				// insert one line from the input file into the database
				start = System.currentTimeMillis();
				
				// insert the authors
				for (int i = 0; i < numAuthors; i++) {
					try {
						pstmtAuthor.setString(1, authorID[i]);
						pstmtAuthor.setString(2, authorFirstName[i]);
						pstmtAuthor.setString(3, authorLastName[i]);

						pstmtAuthor.execute();

					} catch (SQLException e) {

						if (e.getSQLState().equals("23505")) {
							System.out
									.printf("Author %s %s already inserted, skip it. [%s]%n",
											authorFirstName[i], authorLastName[i],
											e.getMessage());
						} else {
							System.out
									.printf("Unrecoverable error while inserting author %s %s:%n",
											authorFirstName[i], authorLastName[i]);
							System.out.printf("- Message: %s%n", e.getMessage());
							System.out.printf("- SQL status code: %s%n", e.getSQLState());
							System.out.printf("- SQL error code: %s%n", e.getErrorCode());
							System.out.printf("%n");
						}
					}
				}

				// insert the publisher
				try {
					pstmtPublisher.setString(1, publisherID);
					pstmtPublisher.setString(2, publisherName);

					pstmtPublisher.execute();

				} catch (SQLException e) {

					if (e.getSQLState().equals("23505")) {
						System.out
							.printf("Publisher %s already inserted, skip it. [%s]%n",
								publisherName, e.getMessage());
					} else {
						System.out
							.printf("Unrecoverable error while inserting publisher %s:%n",
								publisherName);
						System.out.printf("- Message: %s%n", e.getMessage());
						System.out.printf("- SQL status code: %s%n", e.getSQLState());
						System.out.printf("- SQL error code: %s%n", e.getErrorCode());
						System.out.printf("%n");
					}
				}

				// insert the reference
				try {
					pstmtReference.setString(1, referenceID);
					pstmtReference.setString(2, referenceTitle);
					pstmtReference.setInt(3, referenceYear);
					pstmtReference.setString(4, publisherID);

					pstmtReference.execute();

				} catch (SQLException e) {
					System.out
						.printf("Unrecoverable error while inserting reference %s:%n",
								referenceTitle);
					System.out.printf("- Message: %s%n", e.getMessage());
					System.out.printf("- SQL status code: %s%n", e.getSQLState());
					System.out.printf("- SQL error code: %s%n", e.getErrorCode());
					System.out.printf("%n");					
				}

				// link authors to their references
				for (int i = 0; i < numAuthors; i++) {
					try {
						pstmtWrite.setString(1, referenceID);
						pstmtWrite.setString(2, authorID[i]);
						pstmtWrite.setInt(3, i + 1);

						pstmtWrite.execute();

					} catch (SQLException e) {
						System.out
							.printf("Unrecoverable error while linking author %s %s to reference %s:%n",
									authorFirstName[i], authorLastName[i], referenceTitle);
						System.out.printf("- Message: %s%n", e.getMessage());
						System.out.printf("- SQL status code: %s%n", e.getSQLState());
						System.out.printf("- SQL error code: %s%n", e.getErrorCode());
						System.out.printf("%n");						
					}
				}

				if (hasInfo) {

					// insert additional information
					try {
						pstmtInformation.setString(1, referenceID);
						pstmtInformation.setString(2, additionalInfo);

						pstmtInformation.execute();

					} catch (SQLException e) {
						System.out
							.printf("Unrecoverable error while adding information to reference %s:%n",
									referenceTitle);
						System.out.printf("- Message: %s%n", e.getMessage());
						System.out.printf("- SQL status code: %s%n", e.getSQLState());
						System.out.printf("- SQL error code: %s%n", e.getErrorCode());
						System.out.printf("%n");						
					}

				}
				
				end = System.currentTimeMillis();
				
				System.out.printf(
						"Line %,d successfully inserted into the database in %,d milliseconds.%n%n",
						l, end-start);

				System.out
						.printf("Author(s):%n");
				for (int i = 0; i < numAuthors; i++) {
					System.out.printf(" - [%s] [%s]; author position = %d;%n",
							authorFirstName[i], authorLastName[i], i + 1);
				}
				System.out.printf("Title:%n - %s%n", referenceTitle);
				System.out.printf("Publisher:%n - %s%n", publisherName);
				System.out.printf("Year:%n - %d%n", referenceYear);
				if (hasInfo) {
					System.out.printf("Additional information:%n - %s%n",
							additionalInfo);
				}
				
			}
		} finally {

			// close the scanner and the input file
			s.close();

			System.out.printf("%nInput file successfully closed.%n");

			try {

				// close the statements
				if (pstmtAuthor != null && 
					pstmtPublisher != null &&
					pstmtReference != null &&
					pstmtWrite != null && 
					pstmtInformation != null) {						
					
					start = System.currentTimeMillis();
						
					pstmtAuthor.close();
					pstmtPublisher.close();
					pstmtReference.close();
					pstmtWrite.close();
					pstmtInformation.close();
					
					end = System.currentTimeMillis();

					System.out
						.printf("Prepared statements successfully closed in %,d milliseconds.%n",
								end-start);
				
				}

				// close the connection to the database
				if(con != null) {
					
					start = System.currentTimeMillis();
					
					con.close();
					
					end = System.currentTimeMillis();
					
					System.out
					.printf("Connection successfully closed in %,d milliseconds.%n",
							end-start);
					
				}
				
				System.out.printf("Resources successfully released.%n");

			} catch (SQLException e) {
				System.out.printf("Error while releasing resources:%n");

				// cycle in the exception chain
				while (e != null) {
					System.out.printf("- Message: %s%n", e.getMessage());
					System.out.printf("- SQL status code: %s%n", e.getSQLState());
					System.out.printf("- SQL error code: %s%n", e.getErrorCode());
					System.out.printf("%n");
					e = e.getNextException();
				}
			}

		}

	}
}