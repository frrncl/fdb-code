-- Population of the bibliographic references database
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021-2022, Università degli Studi di Padova, Italia


--
-- Population of the Author table
--
INSERT INTO Author VALUES ('BuckJR','John R.','Buck');
INSERT INTO Author VALUES ('CastelliD','Donatella','Castelli');
INSERT INTO Author VALUES ('CornellG','Gary','Cornell');
INSERT INTO Author VALUES ('GilardiG','Gianni','Gilardi');
INSERT INTO Author VALUES ('HorstmannCS','Cay S.','Horstmann');
INSERT INTO Author VALUES ('OppenheimAV','Alan V.','Oppenheim');
INSERT INTO Author VALUES ('PaganoP','Pasquale','Pagano');
INSERT INTO Author VALUES ('SchaferRW','Ronald W.','Schafer');
INSERT INTO Author VALUES ('TanenbaumAS','Andrew S.','Tanenbaum');


--
-- Population of the Publisher table
--
INSERT INTO Publisher VALUES ('ECDL2002','Proc. Sixth European Conference on Research and Advanced Technology for Digital Libraries (ECDL 2002)');
INSERT INTO Publisher VALUES ('McGrawHill','McGraw-Hill');
INSERT INTO Publisher VALUES ('PrenticeHall','Prentice Hall International Editions');
INSERT INTO Publisher VALUES ('Springer','Springer');


--
-- Population of the Reference table
--
INSERT INTO Reference VALUES ('CastelliPagano2002a','OpenDLib: a Digital Library Service System', 2002, 'ECDL2002');
INSERT INTO Reference VALUES ('CastelliPagano2002b','Foundations of a Multidimensional Query Language for Digital Libraries', 2002, 'ECDL2002');
INSERT INTO Reference VALUES ('ECDL2002','Proc. 6th European Conference on Research and Advanced Technology for Digital Libraries (ECDL 2002)', 2002, 'Springer');
INSERT INTO Reference VALUES ('Gilardi1991','Analisi Uno', 1991,'McGrawHill');
INSERT INTO Reference VALUES ('HorstmannCornell2000','Java 2 tecniche avanzate',2000,'McGrawHill');
INSERT INTO Reference VALUES ('HorstmannCornell2001','Java 2 i fondamenti',2001,'McGrawHill');
INSERT INTO Reference VALUES ('OppenheimSchafer1999','Discrete-Time Signal Processing',1999,'PrenticeHall');
INSERT INTO Reference VALUES ('Tanenbaum1996','Computer Networks',1996,'PrenticeHall');
INSERT INTO Reference VALUES ('Tanenbaum2001','Modern Operating Systems',2001,'PrenticeHall');

--
-- Population of the Write table
--
INSERT INTO Write VALUES ('CastelliPagano2002a','CastelliD',1);
INSERT INTO Write VALUES ('CastelliPagano2002a','PaganoP',2);
INSERT INTO Write VALUES ('CastelliPagano2002b','CastelliD',1);
INSERT INTO Write VALUES ('CastelliPagano2002b','PaganoP',2);
INSERT INTO Write VALUES ('Gilardi1991','GilardiG',1);
INSERT INTO Write VALUES ('HorstmannCornell2000','CornellG',2);
INSERT INTO Write VALUES ('HorstmannCornell2000','HorstmannCS',1);
INSERT INTO Write VALUES ('HorstmannCornell2001','CornellG',2);
INSERT INTO Write VALUES ('HorstmannCornell2001','HorstmannCS',1);
INSERT INTO Write VALUES ('OppenheimSchafer1999','BuckJR',3);
INSERT INTO Write VALUES ('OppenheimSchafer1999','OppenheimAV',1);
INSERT INTO Write VALUES ('OppenheimSchafer1999','SchaferRW',2);
INSERT INTO Write VALUES ('Tanenbaum1996','TanenbaumAS',1);
INSERT INTO Write VALUES ('Tanenbaum2001','TanenbaumAS',1);

--
-- Population of the Information table
--
INSERT INTO Information VALUES ('CastelliPagano2002a','pp. 292 - 308');
INSERT INTO Information VALUES ('CastelliPagano2002b','pp. 251 - 265');
INSERT INTO Information VALUES ('Tanenbaum1996','3rd edition');
INSERT INTO Information VALUES ('Tanenbaum2001','2nd edition');