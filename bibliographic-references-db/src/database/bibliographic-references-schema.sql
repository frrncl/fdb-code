-- Creation of the schema for the bibliographic references database
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021-2022, Università degli Studi di Padova, Italia



-- #################################################################################################
-- ## Database creation                                                                           ##
-- #################################################################################################

-- Delete any pre-existing instance of the bibliographic references database
DROP DATABASE IF EXISTS BibliographicReferences;

-- Create database
CREATE DATABASE BibliographicReferences ENCODING 'UTF-8';
COMMENT ON DATABASE BibliographicReferences IS ' Example database for bibliographic references.';

-- #################################################################################################
-- ## Database connection                                                                         ##
-- #################################################################################################

-- Connect to the database
\connect bibliographicreferences

-- #################################################################################################
-- ## Domains creation                                                                            ##
-- #################################################################################################

--
-- This domain is used to identify entities 
--
-- Version 1.00
CREATE DOMAIN Identifier AS VARCHAR(32);
COMMENT ON DOMAIN Identifier IS 'Domain used for entity identifiers.';


-- #################################################################################################
-- ## Tables creation                                                                             ##
-- #################################################################################################

--
-- This table represents the author of a bibliographic reference
--
-- Version 1.00
CREATE TABLE Author(                                                                                                                          
    id Identifier,
    name VARCHAR(30),
    surname VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

  
COMMENT ON TABLE Author IS 'Represents the author of a bibliographic reference.';
COMMENT ON COLUMN Author.id IS 'Unique identifier for the author.';
COMMENT ON COLUMN Author.name IS 'Author''s name.';
COMMENT ON COLUMN Author.surname IS 'Author''s surname.';



--
-- This table represents the publisher of a bibliographic reference
--
-- Version 1.00
CREATE TABLE Publisher(
    id Identifier PRIMARY KEY,
    name VARCHAR(150) Not NUll
);

COMMENT ON TABLE Publisher IS 'Represents the publisher of a bibliographic reference.';
COMMENT ON COLUMN Publisher.id IS 'Unique identifier for the publisher.';
COMMENT ON COLUMN Publisher.name IS 'Publisher''s name.';



--
-- This table represents the bibliographic reference
--
-- Version 1.00
CREATE TABLE Reference(
    id Identifier,            
    title VARCHAR(200) NOT NUll,
    year SMALLINT NOT NULL CHECK (year>0),
    publisher Identifier,
    PRIMARY KEY (id),
    FOREIGN KEY (publisher) REFERENCES Publisher(id)
);

COMMENT ON TABLE Reference IS 'Represents the bibliographic reference.';
COMMENT ON COLUMN Reference.id IS 'Unique identifier for the bibliographic reference.';
COMMENT ON COLUMN Reference.title IS 'Bibliographic reference title.';
COMMENT ON COLUMN Reference.year IS 'Publication year of the bibliographic reference.';
COMMENT ON COLUMN Reference.publisher IS 'Unique identifier for the publisher of the bibliographic reference.';




--
-- This table associates a bibliographic reference with its authors
--
-- Version 1.00
CREATE TABLE Write(
    reference Identifier,
    author Identifier, 
    AuthorNumber SMALLINT DEFAULT 1 NOT NULL CHECK (AuthorNumber>0),
    PRIMARY KEY (reference, author),
    FOREIGN KEY (reference) REFERENCES Reference(id),
    FOREIGN KEY (author) REFERENCES Author(id)
);


COMMENT ON TABLE Write IS 'Associates a bibliographic reference with its authors.';
COMMENT ON COLUMN Write.reference IS 'Unique identifier for the bibliographic reference.';
COMMENT ON COLUMN Write.author IS 'Unique identifier for the author.';
COMMENT ON COLUMN Write.AuthorNumber IS 'Author''s position within the list of authors.';


--
-- This table represents additional information about a bibliographic reference
--
-- Version 1.00
CREATE TABLE Information(
    reference Identifier, 
    notes VARCHAR(200) NOT NULL,
    PRIMARY KEY(reference),
    FOREIGN KEY (reference) REFERENCES Reference(id)
);

COMMENT ON TABLE Information IS 'Represents additional information about a bibliographic reference.';
COMMENT ON COLUMN Information.reference IS 'Unique identifier for the bibliographic reference.';
COMMENT ON COLUMN Information.notes IS 'Bibliographic reference notes.';