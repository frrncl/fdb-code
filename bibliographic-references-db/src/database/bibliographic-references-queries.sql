-- Queries on bibliographic references database
-- 
-- Author: Stefano Marchesin (stefano.marchesin@unipd.it)
-- Version 1.00
--
-- Copyright (c) 2021-2022, Università degli Studi di Padova, Italia


--
-- List name and surname of all the authors
--

SELECT name, surname
  FROM Author;


--
-- List the content of Publisher table
--

SELECT *
  FROM Publisher;


--
-- List, without duplicates, author's identifier and position within the authors list 
--

SELECT DISTINCT author, AuthorNumber
  FROM Write;


--
-- Build a table with a single column named AuthorData, containing the concatenation of surname and name 
--

SELECT surname || ' ' || name AS AuthorData
  FROM Author;


--
-- Tell what's the name of the author whose surname is Gilardi
--

SELECT name
  FROM Author
  WHERE surname = 'Gilardi';


--
-- List identifier and title of bibliographic references that have been published in 1999 and 2000, sorting them by title (ascending)
--

SELECT id, title
  FROM Reference
  WHERE year = 1999 OR year = 2000
  ORDER BY title ASC;


--
-- List name and surname of the authors whose surname starts with the letter C
--

SELECT name, surname
  FROM Author
  WHERE surname LIKE 'C%';


--
-- Find the maximum number of authors for each bibliographic reference, naming the result MaxNumberAuthors
--

SELECT MAX(AuthorNumber) AS NumeroMassimoAutori
  FROM Write;


--
-- Count the number of rows of the Author table, naming  the result AuthorsRowNumber
--

SELECT COUNT(*) AS AuthorsRowNumber
  FROM Author;
  

--
-- Count the number of unique authors within the Write table, naming the result AuthorsNumberWrite 
--

SELECT COUNT(DISTINCT author) AS AuthorsNumberWrite
  FROM Write;


--
-- Group rows in Write table based on author's identifier, showing author's identifier 
--

SELECT author
  FROM Write
  GROUP BY author;


--
-- For each author, represented through the author's identifier, count how many references they published 
--

SELECT author, COUNT(*)
  FROM Write
  GROUP BY author;


--
-- For each author that published more than a bibliographic reference, count how many references they published 
--

SELECT author, COUNT(*) 
  FROM Write
  GROUP BY author HAVING COUNT(*) > 1;


--
-- For each author that is not the first author of a bibliographic reference, count how many references they published
--

SELECT author, COUNT(*) 
  FROM Write
  WHERE AuthorNumber > 1
  GROUP BY author;
  
  
--
-- For each author that is not the first author, and that published more than one bibliographic reference, count how many references they published
--

SELECT author, COUNT(*) 
  FROM Write
  WHERE AuthorNumber > 1
  GROUP BY author HAVING COUNT(*) > 1;


--
--  List title of bibliographic references published by McGraw-Hill publisher
--

SELECT name, title
  FROM Reference AS R INNER JOIN Publisher AS P ON R.publisher = P.id
  WHERE name = 'McGraw-Hill';


--
-- Count for each publisher, identified by their name, the number of bibliographic references they published
--

SELECT name, COUNT(*) 
  FROM Reference AS R INNER JOIN Publisher AS P ON R.publisher = P.id
  GROUP BY name;


--
-- List author's surname and title of all their bibliographic references
--

SELECT surname, title
  FROM Author AS A INNER JOIN Write AS W ON A.id = W.author 
       INNER JOIN Reference AS R ON W.reference = R.id;


--
-- List the titles of all the bibliographic references plus additional information (if any)
--

SELECT title, notes
  FROM Reference AS R LEFT JOIN Information AS I ON R.id = I.reference;


--
-- Count the number of bibliographic references without additional information
--

SELECT COUNT(*) 
  FROM Reference AS R LEFT JOIN Information AS I ON R.id = I.reference
  WHERE notes IS NULL;


--
-- For each publisher, count the number of references each author published, ordering by publisher's name, decreasing number of publications, and author's surname 
-- ordinando per nome editore, numero pubblicazioni decrescente e cognome autore
--

SELECT P.name, A.surname, COUNT(R.id) AS ReferenceNumber
  FROM Reference AS R INNER JOIN Publisher AS P ON R.publisher = P.id
       INNER JOIN Write AS W ON R.id = W.reference
       INNER JOIN Author AS A ON W.author = A.id
   GROUP BY P.name, A.surname
   ORDER BY P.name ASC, ReferenceNumber DESC, A.surname ASC;

--
-- Determine the maximum number of bibliographic references published by a publisher
--

SELECT MAX(ReferenceNumber)
  FROM (SELECT publisher, COUNT(id) AS ReferenceNumber
          FROM Reference
          GROUP BY publisher) AS Counter;

--
-- Determine the publisher that published the maximum number of bibliographic references
--

SELECT publisher, ReferenceNumber
  FROM (SELECT publisher, COUNT(id) AS ReferenceNumber
          FROM Reference
          GROUP BY publisher) AS Counter
  WHERE ReferenceNumber = (SELECT MAX(ReferenceNumber)
                               FROM (SELECT publisher, COUNT(id) AS ReferenceNumber
                                       FROM Reference
                                       GROUP BY publisher) AS Counter2);


--
-- Determine the average number of bibliographic references published by a publisher
--

SELECT AVG(ReferenceNumber)
  FROM (SELECT publisher, COUNT(id) AS ReferenceNumber
          FROM Reference
          GROUP BY publisher) AS Counter;
          
-- or

SELECT CAST( COUNT(id) AS DOUBLE PRECISION) / COUNT(DISTINCT publisher)
  FROM Reference;


--
-- Determine the identifier of the bibliographic references without additional notes
--

SELECT id
  FROM Reference
EXCEPT
SELECT reference
   FROM Information;